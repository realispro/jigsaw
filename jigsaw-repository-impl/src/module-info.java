import jigsaw.repository.impl.JigsawRepositoryImpl;
import jigsaw.repository.JigsawRepository;

module jigsaw.repository.impl {

    requires jigsaw.repository;

    provides JigsawRepository with JigsawRepositoryImpl;

}