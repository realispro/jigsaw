package jigsaw.client;

import jigsaw.service.JigsawService;

public class JigsawClient {

    public static void main(String[] args) {
        System.out.println("JigsawClient.main");

        JigsawService service = new JigsawService();
        String citation = service.getCitation().get();
        System.out.println("citation = " + citation);
        
    }
}
