package jigsaw.service;

import jigsaw.repository.JigsawRepository;

import java.util.Optional;
import java.util.ServiceLoader;

public class JigsawService {

    private JigsawRepository repo;

    public JigsawService(){
        ServiceLoader<JigsawRepository> sl =  ServiceLoader.load(JigsawRepository.class);
        repo = sl.findFirst().get();
    }

    public Optional<String> getCitation(){
        return Optional.of(repo.getCite());
    }

}
