import jigsaw.repository.JigsawRepository;

module jigsaw.service {

    requires jigsaw.repository;
    requires jigsaw.repository.impl;

    exports jigsaw.service;

    uses JigsawRepository;
}